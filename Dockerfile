FROM node:18

EXPOSE 3000

WORKDIR /app

#Cài npm mới nhất
RUN npm i npm@latest -g

#Copy 2 file vào thư mục Gốc
COPY package.json package-lock.json ./

#Cài đặt các thư viện vào folder node_modules
RUN npm install

#Copy hết
COPY . .

#Thực hiện CMD node tại file server.js <=> Chạy CMD câu node server.js
CMD ["node", "server.js"]